### [LookMovie](https://github.com/warren-bank/crx-LookMovie/tree/webmonkey-userscript/es5)

[Userscript](https://github.com/warren-bank/crx-LookMovie/raw/webmonkey-userscript/es5/webmonkey-userscript/LookMovie.user.js) for [lookmovie2.to](https://lookmovie2.to/) to run in:
* the [WebMonkey](https://github.com/warren-bank/Android-WebMonkey) application
  - for Android
* the [Tampermonkey](https://www.tampermonkey.net/) web browser extension
  - for [Firefox/Fenix](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)
  - for [Chrome/Chromium](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)
* the [Violentmonkey](https://violentmonkey.github.io/) web browser extension
  - for [Firefox/Fenix](https://addons.mozilla.org/firefox/addon/violentmonkey/)
  - for [Chrome/Chromium](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag)

Its purpose is to:
* redirect embedded videos to an external player

#### [Domains](https://proxymirrorlookmovie.github.io/):

* active:
  - [**lookmovie2.to**](https://lookmovie2.to/)
  - [lookmovie2.la](https://lookmovie2.la/)
* 301 redirect:
  - [lookmovie.ag](https://lookmovie.ag/)
  - [lookmovie.art](https://lookmovie.art/)
  - [lookmovie.buzz](https://lookmovie.buzz/)
  - [lookmovie.click](https://lookmovie.click/)
  - [lookmovie.clinic](https://lookmovie.clinic/)
  - [lookmovie.com](https://lookmovie.com/)
  - [lookmovie.digital](https://lookmovie.digital/)
  - [lookmovie.download](https://lookmovie.download/)
  - [lookmovie.foundation](https://lookmovie.foundation/)
  - [lookmovie.fun](https://lookmovie.fun/)
  - [lookmovie.fyi](https://lookmovie.fyi/)
  - [lookmovie.guru](https://lookmovie.guru/)
  - [lookmovie.media](https://lookmovie.media/)
  - [lookmovie.mobi](https://lookmovie.mobi/)
  - [lookmovie.site](https://lookmovie.site/)
* disabled:
  - [lookmovie.io](https://lookmovie.io/)

- - - -

#### Bonus:

[LookMovie.css userscript](https://github.com/warren-bank/crx-LookMovie/raw/webmonkey-userscript/es5/webmonkey-userscript/LookMovie.css.user.js)

Its purpose is to apply the following CSS updates:
* hide the upsell popup for "Premium Membership" subscription

- - - -

#### Legal:

* copyright: [Warren Bank](https://github.com/warren-bank)
* license: [GPL-2.0](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)
